package com.gjp.etfweb.form;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class QueryForm {
    // 基金代码
    private String code = "163407";
    // 是否获取最新数据 （true会删除本地缓存文件，重新下载）
    private Boolean isNew = false;
    // 每隔多少天定投一次：日定投填1，周定投填5，月定投填21.75
    private BigDecimal dayInterval = BigDecimal.valueOf(1);
    // 每次定投金额
    private BigDecimal buyAmount = BigDecimal.valueOf(100);
    // 止盈点 (期待的年化收益率)
    private BigDecimal stopPoint = BigDecimal.valueOf(0.15);
    // 申购手续费费率 一般都是1折 0.0012 左右
    private BigDecimal serviceChargeRate = BigDecimal.valueOf(0);
    // 入场时间
    private String startTime;
}
