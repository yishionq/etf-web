package com.gjp.etfweb.common;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class Constant {

    public static final String FILE_ROOT = System.getProperty("user.dir") + File.separator + "基金历史净值-web";
    //计算结果精确到小数点后几位
    public static final Integer DECIMAL_POINT = 5;

    /**
     * 代理IP和端口每日地址
     * https://www.zdaye.com/dayProxy/ip/320494.html
     */
    //是否开启代理
    public static final Boolean PROXY_OPEN = false;
    //代理IP 代理端口 不适用则置空
    public static final String PROXY_HOST = "106.14.173.173";
    public static final Integer PROXY_PORT = 8080;

    // 本地VPN出口
//    public static final String PROXY_HOST = "127.0.0.1";
//    public static final Integer PROXY_PORT = 1080;

    /**
     * 获取请求头
     *
     * @return
     */
    public static Map<String, String> getRequestPropertyMap() {
        //设置请求头
        Map<String, String> requestPropertyMap = new HashMap<>(10);
        requestPropertyMap.put("Accept", "application/json, text/plain, */*");
        requestPropertyMap.put("Connection", "keep-alive");
        requestPropertyMap.put("Origin", "https://www.doctorxiong.club");
        requestPropertyMap.put("Host", "api.doctorxiong.club");
        requestPropertyMap.put("Referer", "https://www.doctorxiong.club/");
        requestPropertyMap.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.92 Safari/537.36");
        return requestPropertyMap;
    }
}
