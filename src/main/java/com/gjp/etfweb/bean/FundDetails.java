package com.gjp.etfweb.bean;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 基金相关详情
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FundDetails {
    /**
     * 日期
     */
    private Date date;

    /**
     * 基金净值
     */
    private BigDecimal netWorth;

    /**
     * 涨跌幅
     */
    private BigDecimal increaseRate;

    /**
     * 购买金额
     */
    private BigDecimal buyAmount;

    /**
     * 购买份额
     */
    private BigDecimal share;

    /**
     * 累计购买金额
     */
    private BigDecimal totalBuyAmount;

    /**
     * 累计购买份额
     */
    private BigDecimal totalShare;

    /**
     * 定投总天数
     */
    private BigDecimal totalDay;

    /**
     * 年化收益率
     */
    private BigDecimal yearEarningsRate;

    /**
     * 计算定投收益 (基金净值-平均成本)*累计定投份额，或总市值-累计定投金额。
     */
    private BigDecimal earningsAmount;

    /**
     * 计算定投收益率 (定投收益/累计定投金额)
     */
    private BigDecimal earningsRate;

    /**
     * 计算平均成本 (累计定投金额/累计定投份额)
     */
    private BigDecimal averageCost;

}
