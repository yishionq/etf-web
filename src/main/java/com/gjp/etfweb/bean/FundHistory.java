package com.gjp.etfweb.bean;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 基金相关
 */
@Data
public class FundHistory {
    /**
     * 基金代码
     */
    private String code;
    /**
     * 基金名称
     */
    private String name;
    /**
     * 日期
     */
    private Date date;

    /**
     * 基金净值
     */
    private BigDecimal netWorth;

    /**
     * 涨跌幅
     */
    private BigDecimal increaseRate;
}
