package com.gjp.etfweb.bean;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class FundPortfolioRoot {
    /**
     * 初始购买总金额
     */
    BigDecimal initBuyAmount = BigDecimal.valueOf(140);
    /**
     * 每隔多少天定投一次：日定投填1，周定投填5，月定投填21.75
     */
    BigDecimal dayInterval = BigDecimal.valueOf(1);
    /**
     * 当前净值 默认净值为1
     */
    BigDecimal netWorth = BigDecimal.ONE;
    /**
     * 总投入成本
     */
    BigDecimal totalCost = BigDecimal.ZERO;
    /**
     * 总投入收益
     */
    BigDecimal totalEarnings = BigDecimal.ZERO;
    /**
     * 基金组合
     */
    private List<FundPortfolio> fundList;
    /**
     * 净值历史
     */
    private List<BigDecimal> netWorthHistory;
}
