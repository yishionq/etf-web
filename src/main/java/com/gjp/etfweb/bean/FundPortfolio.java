package com.gjp.etfweb.bean;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class FundPortfolio {
    /**
     * 基金代码
     */
    private String code;
    /**
     * 基金占比
     */
    private BigDecimal proportion;
    /**
     * 申购手续费费率
     */
    private BigDecimal serviceChargeRate;
    /**
     * 入场时间
     */
    private String startTime;
    /**
     * 该基金所有数据
     */
    private FundInfo fundInfo;
}
