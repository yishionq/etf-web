package com.gjp.etfweb.business;

import com.gjp.etfweb.abstracts.AbstractPolicy;
import com.gjp.etfweb.bean.FundDetails;
import com.gjp.etfweb.bean.FundHistory;
import com.gjp.etfweb.bean.FundInfo;
import com.gjp.etfweb.form.QueryForm;
import com.gjp.etfweb.utils.CommonUtils;
import com.google.common.collect.Lists;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.File;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class CommonBusiness {

    public FundInfo processing(QueryForm form, String fileRoot, AbstractPolicy policy) {
        // 初始化
        policy.init(form);

        // 检测目录
        File root = new File(fileRoot + File.separator + CommonUtils.dateToString(new Date()));
        if (!root.exists()) {
            root.mkdirs();
        }
        // 处理数据
        List<FundDetails> fundDetailsList = Lists.newArrayList();
        String name = form.getCode();
        for (FundHistory fundHistory : policy.pullData(root.getPath())) {
            name = fundHistory.getName();
            //从开始日期开始计算
            if (!StringUtils.isEmpty(form.getStartTime()) && fundHistory.getDate().compareTo(CommonUtils.parseDate(form.getStartTime())) < 0) {
                continue;
            }
            // 计算收益
            policy.processRate(fundHistory);
            // 处理流程
            policy.processBusiness(fundHistory);
            // 记录处理结果
            FundDetails fundDetails = policy.getFundDetails(fundHistory);
            fundDetailsList.add(fundDetails);
        }
        FundInfo fundInfo = new FundInfo();
        fundInfo.setName(name);
        BeanUtils.copyProperties(policy, fundInfo);
        fundInfo.setDetailsList(fundDetailsList);
        return fundInfo;
    }

    private void printDetails(FundDetails fundDetails) {
        System.out.print("日期：");
        System.out.print(CommonUtils.dateFormat.format(fundDetails.getDate()));
        System.out.print("\t");
        System.out.print("基金净值：");
        System.out.print(CommonUtils.numberFormat.format(fundDetails.getNetWorth()));
        System.out.print("\t");
        System.out.print("定投金额：");
        System.out.print(fundDetails.getBuyAmount().stripTrailingZeros().toPlainString());
        System.out.print("\t");
        System.out.print("总定投金额：");
        System.out.print(fundDetails.getTotalBuyAmount().stripTrailingZeros().toPlainString());
        System.out.print("\t");
        System.out.print("总定投份额：");
        System.out.print(CommonUtils.numberFormat.format(fundDetails.getTotalShare()));
        System.out.print("\t");
        System.out.print("平均成本：");
        System.out.print(fundDetails.getAverageCost().stripTrailingZeros().toPlainString());
        System.out.print("\t");
        System.out.print("涨跌：");
        System.out.print(CommonUtils.percentFormat.format(fundDetails.getIncreaseRate().divide(BigDecimal.valueOf(100))));
        System.out.print("\t");
        System.out.print("定投收益率：");
        System.out.print(CommonUtils.percentFormat.format(fundDetails.getEarningsRate()));
        System.out.print("\t");
        System.out.print("年化收益率：");
        System.out.print(CommonUtils.percentFormat.format(fundDetails.getYearEarningsRate()));
        System.out.print("\t");
        System.out.print("定投收益：");
        System.out.print(CommonUtils.numberFormat.format(fundDetails.getEarningsAmount()));
        System.out.println();
    }
}
