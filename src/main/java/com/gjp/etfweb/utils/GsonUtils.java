package com.gjp.etfweb.utils;

import com.google.gson.Gson;

/**
 * @author 郭金鹏
 * @version 1.0
 * @date 2019/10/22 14:10
 */
public class GsonUtils {
    public static final Gson GSON = new Gson()
            .newBuilder()
            .setDateFormat("yyyy-MM-dd HH:mm:ss")
            .create();
}
