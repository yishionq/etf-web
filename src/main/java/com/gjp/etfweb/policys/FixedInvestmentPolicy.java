package com.gjp.etfweb.policys;

import com.gjp.etfweb.abstracts.AbstractPolicy;
import com.gjp.etfweb.bean.FundHistory;
import com.gjp.etfweb.common.Constant;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * 定投策略
 */
@Service
public class FixedInvestmentPolicy extends AbstractPolicy {

    @Override
    public Map<String, BigDecimal> buyPoint(FundHistory fundHistory) {
        Map<String, BigDecimal> buyAndShareMap = new HashMap<>();
        // 从父类获取查询参数
        BigDecimal buyAmount = queryParam.getBuyAmount();
        // 间隔天数+1
        totalDayInterval = totalDayInterval.add(BigDecimal.ONE);
        // 如果达到定投日，则买入
        if (totalDayInterval.compareTo(queryParam.getDayInterval()) >= 0) {
            // 计算定投份额 (定投金额/1+手续费/基金净值)
            BigDecimal buyShare = buyAmount
                    .divide(BigDecimal.ONE.add(queryParam.getServiceChargeRate()), Constant.DECIMAL_POINT, BigDecimal.ROUND_HALF_UP)
                    .divide(fundHistory.getNetWorth(), Constant.DECIMAL_POINT, BigDecimal.ROUND_HALF_UP);
            // 记录购买金额和份额
            buyAndShareMap.put("buy", buyAmount);
            buyAndShareMap.put("share", buyShare);
            // 重置间隔天数
            totalDayInterval = BigDecimal.ZERO;
        }
        return buyAndShareMap;
    }
}
