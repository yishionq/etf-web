package com.gjp.etfweb.policys;

import com.gjp.etfweb.bean.FundHistory;
import com.gjp.etfweb.common.Constant;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * 定投+越跌越买 混合策略
 */
@Service
public class MergePolicy extends MoreFallMoreBuyPolicy {
    @Override
    public Map<String, BigDecimal> buyPoint(FundHistory fundHistory) {
        Map<String, BigDecimal> buyAndShareMap = new HashMap<>();
        // 购买基准数量
        BigDecimal factor = BigDecimal.ZERO;
        // 获取当日净值数据
        BigDecimal increaseRate = fundHistory.getIncreaseRate();
        BigDecimal netWorth = fundHistory.getNetWorth();

        // ===================================定投部分======================================
        // 间隔天数+1
        totalDayInterval = totalDayInterval.add(BigDecimal.ONE);
        // 如果达到定投日，则买入一份
        if (totalDayInterval.compareTo(queryParam.getDayInterval()) >= 0) {
            factor = factor.add(BigDecimal.ONE);
            // 重置间隔天数
            totalDayInterval = BigDecimal.ZERO;
        }

        // ===================================越跌越买部分======================================
        //判断涨跌情况
        if (increaseRate.compareTo(BigDecimal.ZERO) < 0) {
            //记录连续跌幅
            continuousDecline = continuousDecline.add(increaseRate);
            // 如果跌幅超过2% 触发买入机制 例如跌幅 -2.1% 或 3.5%
            if (increaseRate.compareTo(BigDecimal.valueOf(-2)) < 0) {
                //计算买入多少单位金额
                factor = increaseRate
                        .add(BigDecimal.valueOf(2))
                        .divide(BigDecimal.valueOf(-1), 0, BigDecimal.ROUND_DOWN)
                        .add(BigDecimal.ONE);
                // 判断当前平均净值跌破
                if (averageCost.compareTo(BigDecimal.ZERO) == 0) {
                    // todo 平均值为0，说明还没上车
                } else if (averageCost.compareTo(netWorth) < 0) {
                    // todo 没跌破平均净值
                } else if (averageCost.compareTo(netWorth) > 0) {
                    // todo 跌破平均值
                    // 计算跌破比  (当前净值/平均净值)-1
//                    BigDecimal averageFall = netWorth
//                            .divide(averageCost, Constant.DECIMAL_POINT, BigDecimal.ROUND_HALF_UP)
//                            .subtract(BigDecimal.ONE);
                    // 在购买数量的基础上，再加额外数量
                    factor = continuousDecline
                            .add(BigDecimal.valueOf(2))
                            .divide(BigDecimal.valueOf(-1), 0, BigDecimal.ROUND_DOWN)
                            .add(BigDecimal.ONE);
                }
            }
        } else {
            // 清空连续跌幅
            continuousDecline = BigDecimal.ZERO;
        }
        // 计算购买金额和购买基金份额
        BigDecimal buyAmount = queryParam.getBuyAmount().multiply(factor);
        // 购买金额不为0
        if (buyAmount.compareTo(BigDecimal.ZERO) > 0) {
            // 计算定投份额 (定投金额/1+手续费/基金净值)
            BigDecimal buyShare = buyAmount
                    .divide(BigDecimal.ONE.add(queryParam.getServiceChargeRate()), Constant.DECIMAL_POINT, BigDecimal.ROUND_HALF_UP)
                    .divide(fundHistory.getNetWorth(), Constant.DECIMAL_POINT, BigDecimal.ROUND_HALF_UP);
            // 记录购买金额和份额
            buyAndShareMap.put("buy", buyAmount);
            buyAndShareMap.put("share", buyShare);
        }
        return buyAndShareMap;
    }

}
