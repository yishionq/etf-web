package com.gjp.etfweb.controller;

import com.gjp.etfweb.bean.FundPortfolioRoot;
import com.gjp.etfweb.business.FundPortfolioBusiness;
import com.gjp.etfweb.form.FundPortfolioForm;
import com.gjp.etfweb.form.FundPortfolioRootForm;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 51101
 */
@RestController
public class FundPortfolioController {

    private final FundPortfolioBusiness fundPortfolioBusiness;

    public FundPortfolioController(FundPortfolioBusiness fundPortfolioBusiness) {
        this.fundPortfolioBusiness = fundPortfolioBusiness;
    }

    @GetMapping("/fundPortfolio")
    public ModelAndView test(FundPortfolioRootForm form) {
        String startTime = "2020-04-27";
        form.setInitBuyAmount(BigDecimal.valueOf(140));
        ModelAndView modelAndView = new ModelAndView("fundPortfolioPage");
        List<FundPortfolioForm> fundList = new ArrayList<>();

        FundPortfolioForm fund1 = new FundPortfolioForm();
        fund1.setCode("007339");
        fund1.setProportion(BigDecimal.valueOf(0.15));
        fund1.setServiceChargeRate(BigDecimal.valueOf(0));
        fund1.setStartTime(startTime);

        FundPortfolioForm fund2 = new FundPortfolioForm();
        fund2.setCode("006479");
        fund2.setProportion(BigDecimal.valueOf(0.15));
        fund2.setServiceChargeRate(BigDecimal.valueOf(0));
        fund2.setStartTime(startTime);

        FundPortfolioForm fund3 = new FundPortfolioForm();
        fund3.setCode("001632");
        fund3.setProportion(BigDecimal.valueOf(0.1));
        fund3.setServiceChargeRate(BigDecimal.valueOf(0));
        fund3.setStartTime(startTime);

        FundPortfolioForm fund4 = new FundPortfolioForm();
        fund4.setCode("002621");
        fund4.setProportion(BigDecimal.valueOf(0.1));
        fund4.setServiceChargeRate(BigDecimal.valueOf(0.0015));
        fund4.setStartTime(startTime);

        FundPortfolioForm fund5 = new FundPortfolioForm();
        fund5.setCode("007883");
        fund5.setProportion(BigDecimal.valueOf(0.1));
        fund5.setServiceChargeRate(BigDecimal.valueOf(0));
        fund5.setStartTime(startTime);

        FundPortfolioForm fund6 = new FundPortfolioForm();
        fund6.setCode("006229");
        fund6.setProportion(BigDecimal.valueOf(0.1));
        fund6.setServiceChargeRate(BigDecimal.valueOf(0));
        fund6.setStartTime(startTime);

        FundPortfolioForm fund7 = new FundPortfolioForm();
        fund7.setCode("001182");
        fund7.setProportion(BigDecimal.valueOf(0.15));
        fund7.setServiceChargeRate(BigDecimal.valueOf(0.001));
        fund7.setStartTime(startTime);

        FundPortfolioForm fund8 = new FundPortfolioForm();
        fund8.setCode("000217");
        fund8.setProportion(BigDecimal.valueOf(0.15));
        fund8.setServiceChargeRate(BigDecimal.valueOf(0));
        fund8.setStartTime(startTime);

        fundList.add(fund1);
        fundList.add(fund2);
        fundList.add(fund7);
        fundList.add(fund8);
        fundList.add(fund3);
        fundList.add(fund4);
        fundList.add(fund5);
        fundList.add(fund6);
        form.setFundList(fundList);
        FundPortfolioRoot fundPortfolioRoot = fundPortfolioBusiness.processing(form);
        modelAndView.addObject("fundPortfolioRoot", fundPortfolioRoot);
        return modelAndView;
    }
}
