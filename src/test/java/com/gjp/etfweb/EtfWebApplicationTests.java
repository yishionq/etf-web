package com.gjp.etfweb;

import com.gjp.etfweb.bean.FundPortfolioRoot;
import com.gjp.etfweb.business.FundPortfolioBusiness;
import com.gjp.etfweb.form.FundPortfolioForm;
import com.gjp.etfweb.form.FundPortfolioRootForm;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class EtfWebApplicationTests {
    @Autowired
    private FundPortfolioBusiness fundPortfolioBusiness;

    @Test
    void contextLoads() {
        FundPortfolioRootForm form = new FundPortfolioRootForm();
        form.setInitBuyAmount(BigDecimal.valueOf(100));
        List<FundPortfolioForm> fundList = new ArrayList<>();
        FundPortfolioForm fund1 = new FundPortfolioForm();
        fund1.setCode("001632");
        fund1.setProportion(BigDecimal.valueOf(0.5));
        fund1.setServiceChargeRate(BigDecimal.ZERO);
        fund1.setStartTime("2020-04-10");
        FundPortfolioForm fund2 = new FundPortfolioForm();
        fund2.setCode("007339");
        fund2.setProportion(BigDecimal.valueOf(0.5));
        fund2.setServiceChargeRate(BigDecimal.ZERO);
        fund2.setStartTime("2020-04-10");
        fundList.add(fund1);
        fundList.add(fund2);
        form.setFundList(fundList);
        FundPortfolioRoot fundPortfolioRoot = fundPortfolioBusiness.processing(form);
        System.out.println(fundPortfolioRoot);
    }

}
